class WithPhysics < Ducky::Scene
  def initialize
    super

    @background_color = Color.duck_blue

    # Insert your nodes here
    register_sprite(Area.new(Vector2.new('70%', '40%')))
    register_sprite(Area.new(Vector2.new('80%', '40%')))
    register_sprite(Area.new(Vector2.new('70%', '50%')))
    register_sprite(Area.new(Vector2.new('80%', '50%')))
    register_sprite(Area.new(Vector2.new('70%', '60%')))
    register_sprite(Area.new(Vector2.new('80%', '60%')))

    register_sprite(Player.new(Vector2.new('50%', '50%')))

    # Scene navigation
    @next_scene = nil # This is mandatory, replace with your next_scene class (Level)
  end

  def complete?
    false
  end

  def must_exit?
    false
  end
end

class Player < Ducky::KinematicBody
  def initialize(position, name: "Player##{hash}")
    super(collision_layer: :player, name: name, position: position)

    add_child(Ducky::Sprite.new(position: Vector2.zero, width: 64, height: 64, name: name))

    add_shape(
      Ducky::RectangleShape.new(
        position: Vector2.zero,
        size: Vector2.new(64, 64)
      )
    )

    enable_physics

    Ducky::Input.register_action(:player_left, :left)
    Ducky::Input.register_action(:player_right, :right)
    Ducky::Input.register_action(:player_up, :up)
    Ducky::Input.register_action(:player_down, :down)
  end

  def update(_args)
    speed = 16

    velocity = Vector2.zero
    velocity.x += 1 if Ducky::Input.action_pressed?(:player_right)
    velocity.x -= 1 if Ducky::Input.action_pressed?(:player_left)
    velocity.y -= 1 if Ducky::Input.action_pressed?(:player_down)
    velocity.y += 1 if Ducky::Input.action_pressed?(:player_up)

    @local_position += velocity.normalized * speed
  end
end

class Area < Ducky::StaticBody
  def initialize(position, name: "Area##{hash}")
    super(collision_layer: :area, position: position, name: name)

    @sprite = add_child(Ducky::Sprite.new(position: Vector2.zero, width: 50, height: 50, name: name,
                                          asset: 'square/green.png'))

    add_shape(
      Ducky::RectangleShape.new(
        position: Vector2.zero,
        size: Vector2.new(50, 50)
      )
    )

    on_collision_with(:player) do |_collision|
      @sprite.asset = 'square/red.png'
    end

    on_collision_lost_with(:player) do |_collision|
      @sprite.asset = 'square/green.png'
    end
  end

  def update(_args)
    # No Op
  end
end
