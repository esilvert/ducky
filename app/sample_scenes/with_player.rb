class WithPlayer < Ducky::Scene
  def initialize
    super

    @background_color = Color.duck_blue

    # Insert your nodes here


    # Scene navigation
    @next_scene = nil # This is mandatory, replace with your next_scene class (Level)
    register_sprite(Player.new(Vector2.new('50%', '50%')))
  end

  def complete?
    false
  end

  def must_exit?
    false
  end
end

class Player < Ducky::Sprite
  def initialize(position, name: "Player##{hash}")
    super(position: position, width: 64, height: 64, name: name)

    Ducky::Input.register_action(:player_left, :left)
    Ducky::Input.register_action(:player_right, :right)
    Ducky::Input.register_action(:player_up, :up)
    Ducky::Input.register_action(:player_down, :down)
  end

  def update(_args)
    speed = 32

    velocity = Vector2.zero
    velocity.x += 1 if Ducky::Input.action_pressed?(:player_right)
    velocity.x -= 1 if Ducky::Input.action_pressed?(:player_left)
    velocity.y -= 1 if Ducky::Input.action_pressed?(:player_down)
    velocity.y += 1 if Ducky::Input.action_pressed?(:player_up)

    @local_position += velocity.normalized * speed
  end
end
