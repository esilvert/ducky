module Ducky
  class Node
    class NotANode < StandardError; end

    module Type
      LABEL = :label
      SPRITE = :sprite
      PANEL = :sprite
      BUTTON = :sprite
      STATIC_BODY = :primitive
      KINEMATIC_BODY = :primitive
      BACKGROUND_COLOR = :background_color
    end

    attr_reader :parent, :children
    attr_accessor :name

    def initialize(name = "#{self.class.name}##{hash}")
      @name = name
      @children = []
    end

    def serialize
      res = {}

      return name
      # instance_variables.each do |var_name|
      #   next if instance_variable_get(var_name).is_a?(Node)
      #
      #   var = var_name.to_s.split('@')[1]
      #   res[var] = instance_variable_get(var_name)
      # end

      res
    end

    def inspect
      serialize.to_s
    end

    def to_s
      serialize.to_s
    end

    def parent=(node)
      if !node.is_a?(Node) && !node.nil?
        raise NotANode, "Ducky::Node#parent= must receive a Ducky::Node, received <#{node}>"
      end

      @parent = node
    end

    def add_child(node)
      raise NotANode, "Ducky::Node#add_child must receive a Ducky::Node, received <#{node}>" unless node.is_a?(Node)

      @children << node
      node.parent = self

      node
    end

    # Called generically by the system
    def system_update(args)
      ducky_update(args) # Uses internally by ducky nodes
      update(args) # Can be overriden by end user
      @children.each { |child| child.system_update(args) }
    end

    def ducky_update(args); end
    def update(args); end

    def system_draw(graphics, args)
      draw(graphics, args)
      @children.each do |node|
        node.system_draw(graphics, args)
      end
    end

    def draw(_args)
      raise NotImplementedError, "You must define a #draw method for node #{name}"
    end
  end
end
