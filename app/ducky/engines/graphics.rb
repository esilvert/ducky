module Ducky
  module Engine
    class Graphics
      def initialize
        @camera = { x: 0, y: 0 }
      end

      def draw(space, node)
        raise "node #{node.name} is not a hash" unless node.to_dr.is_a?(Hash)

        node_dr = translate_to_camera(node.to_dr)

        @args.outputs.send(space) << node_dr
      end

      def system_update(args)
        @args = args
      end

      def self.clear
        $gtk.args.outputs.static_solids.clear
        $gtk.args.outputs.static_sprites.clear
        $gtk.args.outputs.static_primitives.clear
        $gtk.args.outputs.static_labels.clear
        $gtk.args.outputs.static_lines.clear
        $gtk.args.outputs.static_borders.clear
        $gtk.args.outputs.static_debug.clear
      end

      def translate_to_camera(node_dr)
        return node_dr unless node_dr.key?(:x) && node_dr.key?(:y)

        node_dr.merge!(
          x: node_dr[:x] - @camera[:x],
          y: node_dr[:y] - @camera[:y]
        )
      end
    end
  end
end
