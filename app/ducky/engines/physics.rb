module Ducky
  module Engine
    class Physics
      module ClassMethods
        attr_accessor :debug

        def instance
          @instance ||= new
        end

        def register_body(body)
          instance.register_body(body)
        end

        def clear
          instance.clear
        end
      end
      extend ClassMethods

      def register_body(body)
        raise ArgumentError, 'Physics#register_body must receive a Ducky::Physics2D' unless body.is_a?(Physics2D)

        log "Registered a body #{body.class.name} (#{@bodies.count + 1})"
        @bodies << body
      end

      def system_update(_args)
        new_collisions = process_collisions

        process_lost_collisions(new_collisions)

        @previous_collisions = new_collisions
      end

      def clear
        @bodies = []
        @bodies_map = {}
        @previous_collisions = Hash.new { |hash, new_key| hash[new_key] = [] }
      end

      private

      def initialize
        clear
      end

      def notify_collision(body, other_body)
        body.collide_with(other_body)
        other_body.collide_with(body)
      end

      def notify_collision_lost(body, other_body)
        body.no_longer_collide_with(other_body)
        other_body.no_longer_collide_with(body)
      end

      def compute_body_map
        @bodies.to_h { |body| [body.to_dr, body] }
      end

      def compute_collisions(bodies_dr)
        GTK::Geometry.find_collisions bodies_dr.keys
      end

      def process_collisions
        bodies_dr = compute_body_map
        collisions = compute_collisions(bodies_dr)
        new_collisions = Hash.new { |hash, new_key| hash[new_key] = [] }

        collisions.each do |body_dr, other_body_dr|
          body = bodies_dr[body_dr]
          other_body = bodies_dr[other_body_dr]

          new_collisions[body] << other_body

          log "Collision detected between #{body.name} and #{other_body.name}"
          notify_collision(body, other_body)
        end

        new_collisions
      end

      def process_lost_collisions(new_collisions)
        @previous_collisions.each do |body_dr, previous_collisions|
          body_new_collision = new_collisions[body_dr]

          lost_collisions = previous_collisions - body_new_collision

          lost_collisions.each do |other_body|
            log "Collision lost between #{body_dr.name} and #{other_body.name}"
            notify_collision_lost(body_dr, other_body)
          end
        end
      end
    end
  end
end
