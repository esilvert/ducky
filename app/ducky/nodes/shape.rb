module Ducky
  attr_accessor :size
  attr_reader :w, :h

  class Shape < Node2D
    def initialize(position:, size:, name: "Shape##{hash}")
      super(position: position, name: name)
      @size = size
      @w = size.x
      @h = size.y
    end

    def draw(graphics, _args)
      return unless Engine::Physics.debug

      graphics.draw(:borders, self)
    end

    def to_dr
      r = object_id % 255
      g = (object_id + 124) % 255
      b = (object_id + 64) % 255

      { x: position.x, y: position.y, w: @w, h: @h, r: r, g: g, b: b }
    end
  end
end
