module Ducky
  class Physics2D < Node2D
    attr_reader :collision_layer, :shape

    def initialize(position:, collision_layer:, name: "Physics2D##{hash}")
      super(position: position, name: name)

      @collision_layer = collision_layer
      @_registered = false
      @collision_handlers = Hash.new { |hash, new_key| hash[new_key] = [] }
      @collision_lost_handlers = Hash.new { |hash, new_key| hash[new_key] = [] }

      enable_physics
    end

    def collide_with(other_body)
      key = collision_handler_key(collision_layer, other_body.collision_layer)

      @collision_handlers[key].each do |handler|
        handler.call(other_body)
      end
    end

    def no_longer_collide_with(other_body)
      key = collision_handler_key(collision_layer, other_body.collision_layer)

      @collision_lost_handlers[key].each do |handler|
        handler.call(other_body)
      end
    end

    def on_collision_with(other_layer, &block)
      key = collision_handler_key(collision_layer, other_layer)

      @collision_handlers[key] << block
    end

    def on_collision_lost_with(other_layer, &block)
      key = collision_lost_handler_key(collision_layer, other_layer)

      @collision_lost_handlers[key] << block
    end

    def add_shape(shape)
      @shape = add_child(shape)
    end

    def draw(_graphics, _args); end

    def to_dr
      @shape.to_dr
    end

    private

    def enable_physics
      return if @_registered

      Engine::Physics.register_body(self)
      @_registered = true
    end

    def collision_handler_key(first_body_type, other_body_type)
      [first_body_type, other_body_type].sort
    end
    alias collision_lost_handler_key collision_handler_key
  end
end
