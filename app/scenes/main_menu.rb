class MainMenu < Ducky::Scene
  def initialize
    super
    @background_color = Color.black

    register_label(
      Ducky::Label.new(
        text: 'Your Amazing Game',
        position: Vector2.new('50%', '70%'),
        size: 15
      ),
      static: true
    )
    @play_button = register_button(play_button)
    @exit_button = register_button(exit_button)

    @next_scene = Level

    # register_panel(Panel.new(size: Vector2.new(10000,800)), static: true)
  end

  def complete?
    @play_button&.clicked?
  end

  def must_exit?
    @exit_button&.clicked?
  end

  private

  def play_button
      Ducky::Button.new(
        position: Vector2.new('50%', '50%'),
        width: 400,
        height: 100,
        text: 'Play !'
      )

  end

  def exit_button
      Ducky::Button.new(
        position: Vector2.new('50%', '30%'),
        width: 400,
        height: 100,
        text: 'Exit'
      )
  end
end
